<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class LoginController extends Controller
{
    /**
     * @Route("/registro", name="registro")
     */
    public function registroAction(Request $request)
    {        
        return new \Symfony\Component\HttpFoundation\Response("registro");
    }       
    
    
    /**
     * @Route("/activar/{token}", name="activar_usuario")
     */
    public function activarUsuarioAction($token, Request $request)
    {        
        return new \Symfony\Component\HttpFoundation\Response("activar usuario " . $token);
    } 
    
}
