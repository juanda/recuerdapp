<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class NotasController extends Controller {

    /**
     * @Route("/", name="homepage", methods="GET")
     */
    public function indexAction(Request $request) {        
        list($etiquetas, $notas, $nota_seleccionada) = $this->dameEtiquetasYNotas();

        return $this->render('notas/index.html.twig', array(
                    'etiquetas' => $etiquetas,
                    'notas' => $notas,
                    'nota_seleccionada' => $nota_seleccionada,
        ));
    }

    /**
     * @Route("/notas-etiquetadas-con/{etiqueta}", name="notas-etiquetadas-con", methods="GET")
     */
    public function notasEtiquetadasConAction($etiqueta, Request $request) {
        $session = $request->getSession();

        $session->set('busqueda.tipo', 'por_etiqueta');
        $session->set('busqueda.valor', $etiqueta);
        $session->set('nota.seleccionada.id', '');

        list($etiquetas, $notas, $nota_seleccionada) = $this->dameEtiquetasYNotas();

        return $this->render('notas/index.html.twig', array(
                    'etiquetas' => $etiquetas,
                    'notas' => $notas,
                    'nota_seleccionada' => $nota_seleccionada,
        ));
    }

    /**
     * @Route("/buscar", name="buscar", methods="POST")
     */
    public function buscarAction(Request $request) {
        $session = $request->getSession();

        $session->set('busqueda.tipo', 'por_termino');
        $session->set('busqueda.valor', $request->get('termino'));
        $session->set('nota.seleccionada.id', '');

        list($etiquetas, $notas, $nota_seleccionada) = $this->dameEtiquetasYNotas();

        return $this->render('notas/index.html.twig', array(
                    'etiquetas' => $etiquetas,
                    'notas' => $notas,
                    'nota_seleccionada' => $nota_seleccionada,
        ));
    }

    /**
     * @Route("/nota/{id}", name="nota", requirements={"id": "\d+"}, methods="GET")
     */
    public function notaAction($id, Request $request) {
        $session = $request->getSession();

        $session->set('nota.seleccionada.id', $id);

        list($etiquetas, $notas, $nota_seleccionada) = $this->dameEtiquetasYNotas();

        return $this->render('notas/index.html.twig', array(
                    'etiquetas' => $etiquetas,
                    'notas' => $notas,
                    'nota_seleccionada' => $nota_seleccionada,
        ));
    }

    /**
     * @Route("/crear", name="crear", methods="GET|POST")
     */
    public function crearAction(Request $request) {
        $session = $request->getSession();

        if ($request->getMethod() == 'POST') {

            // si los datos que vienen en la request son buenos guarda la nota

            $session
                    ->getFlashBag()->add('mensaje', 'Se debería guardar la nota:'
                    . $request->get('nombre') .
                    '. Como aun no disponemos de un servicio para
                          persistir los datos, mostramos la nota 1');

            return $this
                            ->redirect($this->generateUrl('nota', array('id' => 1)));
        }

        list($etiquetas, $notas, $nota_seleccionada) = $this->dameEtiquetasYNotas();

        return $this->render('notas/crear.html.twig', array(
                    'etiquetas' => $etiquetas,
                    'notas' => $notas,
                    'nota_seleccionada' => $nota_seleccionada,
                        )
        );
    }

    /**
     * @Route("/editar/{id}", name="editar", requirements={"id": "\d+"}, methods="GET|POST")
     */
    public function editarAction($id, Request $request) {
        $session = $request->getSession();

        // Se recupera la nota que viene en la request para ser editada

        $nota = array(
            'id' => $id,
            'titulo' => 'nota',
        );


        if ($request->getMethod() == 'POST') {

            // si los datos que vienen en la request son buenos guarda la nota

            $session->getFlashBag()->add('mensaje', 'Se debería editar la nota:'
                    . $request->get('titulo') .
                    '. Como aún no disponemos de un servicio para persistir los
                         datos, la nota permanece igual');

            return $this->redirect(
                            $this->generateUrl('nota', array('id' => $id)
                            )
            );
        }

        list($etiquetas, $notas, $nota_seleccionada) = $this->dameEtiquetasYNotas();

        return $this
                        ->render('notas/editar.html.twig', array(
                            'etiquetas' => $etiquetas,
                            'notas' => $notas,
                            'nota_a_editar' => $nota,
        ));
    }

    /**
     * @Route("/borrar/{id}", name="borrar", requirements={"id": "\d+"}, methods="GET")
     */
    public function borrarAction($id, Request $request) {         
         $session = $request->getSession();

         // borrado de la nota $request->get('id');

         $session->getFlashBag()->add('mensaje',
             'Se debería borrar la nota ' . $id);

         $session->set('nota.seleccionada.id', '');

         return $this->forward('AppBundle:Notas:index');
    }

    /**
     * @Route("/mi-espacio", name="espacio_premium")
     */
    public function espacioPremiumAction(Request $request) {
        return new \Symfony\Component\HttpFoundation\Response("espacio premium ");
    }

    /**
     * @Route("/rss", name="rss")
     */
    public function rssAction(Request $request) {
        return new \Symfony\Component\HttpFoundation\Response("rss ");
    }

    /**
     * Función Mock para poder desarrollar y probar la lógica de control.
     *
     * La función real que finalmente se implemente, utilizará el filtro
     * almacenado en la sesión y el modelo para calcular la etiquetas, notas
     * y nota seleccionada que en cada momento se deban pintar.
     */
    protected function dameEtiquetasYNotas() {
        $session = $this->get('session');

        $etiquetas = array(
            array(
                'id' => 1,
                'texto' => 'etiqueta 1',
            ),
            array(
                'id' => 2,
                'texto' => 'etiqueta 2',
            ),
            array(
                'id' => 3,
                'texto' => 'etiqueta 3',
            ),
        );

        $notas = array(
            array(
                'id' => 1,
                'titulo' => 'nota 1',
            ),
            array(
                'id' => 2,
                'titulo' => 'nota 2',
            ),
            array(
                'id' => 3,
                'titulo' => 'nota 3',
            ),
        );

        $nota_selecionada_id = $session->get('nota.seleccionada.id');
        if (!$nota_selecionada_id) {
            $nota_selecionada_id = 1;
        }

        $nota_seleccionada = array(
            'id' => $nota_selecionada_id,
            'titulo' => 'nota ' . $nota_selecionada_id,
        );
        return array($etiquetas, $notas, $nota_seleccionada);
    }

}
