<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ContratosController extends Controller
{
    /**
     * @Route("/tarifas", name="tarifas")
     */
    public function tarifasAction(Request $request)
    {        
        return new \Symfony\Component\HttpFoundation\Response("tarifas");
    }       
    
    
    /**
     * @Route("/contratar", name="contratar")
     */
    public function contratarAction(Request $request)
    {        
        return new \Symfony\Component\HttpFoundation\Response("contratar");
    }     
}
